import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*

plugins {
    kotlin("jvm") version "1.3.72"
    id("com.google.cloud.tools.jib") version "2.1.0"
}

val group = "br.com.trustsystems"
val version = "0.1.0-ALPHA.1-SNAPSHOT"
val tag = generateTag(version)

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
}

jib {
    from {
        image = "amazoncorretto@sha256:332c2f24ea056e182312ab5059c7e4f40c73a330d355827181e5d2194001a198"
    }
    to {
        image = "registry.gitlab.com/wendersonferreira/mqtt-fire"
        tags = setOf(tag)

        auth {
            username = System.getenv("REGISTRY_USER")
            password = System.getenv("REGISTRY_KEY")
        }
    }
    container {
        labels = mapOf(
            "maintainer" to "Trustsystems <wenderson@trustsystems.com.br>",
            "org.opencontainers.image.title" to "mqtt-fire",
            "org.opencontainers.image.description" to "designed to test a mqtt server",
            "org.opencontainers.image.version" to tag,
            "org.opencontainers.image.authors" to "Wenderson Ferreira de Souza <wenderson@trustsystems.com.br>",
            "org.opencontainers.image.url" to "https://gitlab.com/wendersonferreira/mqtt-fire",
            "org.opencontainers.image.vendor" to "https://trustsystems.com.br",
            "org.opencontainers.image.licenses" to "MIT"
        )
        creationTime = Instant.now().toString()
        jvmFlags = listOf(
            "-server",
            "-XX:+UseContainerSupport"
        )
        mainClass = "br.com.trustsystems.gravity.FireKt"
    }
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    wrapper {
        gradleVersion = "6.3"
        distributionType = Wrapper.DistributionType.BIN
    }
}

fun generateTag(version: String): String {
    return if (version.endsWith("-SNAPSHOT"))
        version.replace("-SNAPSHOT", "")
            .plus("_").plus(SimpleDateFormat("yyyyMMddHHmmss").format(Date())) else version
}
